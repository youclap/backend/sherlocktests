FROM openjdk:8-jre-alpine


ENV ENVIRONMENT=dev

EXPOSE 80
VOLUME /tmp

COPY build/libs/sherlock.jar app.jar

ENTRYPOINT java -Djava.security.egd=file:/dev/./urandom -jar app.jar --spring.profiles.active=$ENVIRONMENT
