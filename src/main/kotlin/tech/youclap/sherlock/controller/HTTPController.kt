package tech.youclap.sherlock.controller

import org.reactivestreams.Publisher
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import tech.youclap.sherlock.model.SearchResult
import tech.youclap.sherlock.service.SearchServiceRepresentable

@RestController
@RequestMapping("/searchtests")
@CrossOrigin("*")
class HTTPController(
    private val service: SearchServiceRepresentable
) {

    private companion object {
        private val logger = LoggerFactory.getLogger(HTTPController::class.java)
    }

    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun search(@RequestParam(name = "q") query: String): Publisher<SearchResult> {

        logger.info("🔍 Query received with q='$query'")

        return service.search(query)
            .doOnNext {
                logger.info("ℹ️ Returned '${it.profileIDs.size}' usersID")
            }
    }
}
