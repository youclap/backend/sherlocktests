package tech.youclap.sherlock.service

import reactor.core.publisher.Mono
import tech.youclap.sherlock.model.SearchResult

interface SearchServiceRepresentable {

    fun search(term: String): Mono<SearchResult>
}
