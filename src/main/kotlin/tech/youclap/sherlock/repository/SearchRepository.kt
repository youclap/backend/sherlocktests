package tech.youclap.sherlock.repository

import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.index.query.Operator
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import tech.youclap.sherlock.extensions.searchMono
import java.math.BigInteger

@Component
class SearchRepository(
    private val client: RestHighLevelClient
) : SearchRepositoryRepresentable {

    private companion object {
        private val logger = LoggerFactory.getLogger(SearchRepository::class.java)
    }

    override fun findByTerm(term: String): Mono<List<BigInteger>> {

        val query = QueryBuilders.multiMatchQuery(term)
            .field("name")
            .field("username")
            .operator(Operator.AND)

        val searchRequest = SearchRequest("profile")
        val searchSourceBuilder = SearchSourceBuilder()

        searchSourceBuilder.query(query)
        searchSourceBuilder.size(10000)
        searchRequest.source(searchSourceBuilder)

        return client.searchMono(searchRequest)
            .doOnNext { logger.info("⏰ took ${it.took}") }
            .map { searchResponse ->
                searchResponse.hits.hits
                    .map { searchHit ->
                        searchHit.id.toBigInteger()
                    }
            }
    }
}
