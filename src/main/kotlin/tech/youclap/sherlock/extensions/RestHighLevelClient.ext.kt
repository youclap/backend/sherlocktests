package tech.youclap.sherlock.extensions

import org.elasticsearch.action.ActionListener
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import reactor.core.publisher.Mono
import reactor.core.publisher.MonoSink

// TODO find a better name
fun RestHighLevelClient.searchMono(
    searchRequest: SearchRequest,
    options: RequestOptions = RequestOptions.DEFAULT
): Mono<SearchResponse> {

    return Mono.create { sink ->
        this.searchAsync(searchRequest, options, listenerToSink(sink))
    }
}

private fun <Response> listenerToSink(sink: MonoSink<Response>): ActionListener<Response> {
    return object : ActionListener<Response> {
        override fun onResponse(response: Response) {
            sink.success(response)
        }

        override fun onFailure(e: Exception) {
            sink.error(e)
        }
    }
}
